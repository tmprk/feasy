//
//  CardTransitioningDelegate.swift
//  Feasy
//
//  Created by Xara on 12/2/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

class CardTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresentedViewController presented: UIViewController,
                                presenting: UIViewController?,
                                sourceViewController source: UIViewController) -> UIPresentationController? {
        
        return CardPresentationController(presentedViewController: presented,
                                          presenting: presenting)
    }
    
    func animationController(forPresentedController presented: UIViewController, presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CardPresentationAnimator()
    }
    
    func animationController(forDismissedController dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CardDismissalAnimator()
    }
}
