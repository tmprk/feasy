//
//  ComposeAnimation.swift
//  Feasy
//
//  Created by Xara on 12/2/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

extension ComposeController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            menuView.frame.origin.y += calculateWidth() / 3.5
            backdropView.alpha = 0
            UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
                self.menuView.frame.origin.y -= self.calculateWidth() / 3.5
                self.backdropView.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
                self.leftEditableTextField.becomeFirstResponder()
                self.menuView.transform = CGAffineTransform.identity
            })
        } else {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations: {
                self.menuView.frame.origin.y += self.calculateWidth() / 3.5
                self.backdropView.alpha = 0
                self.leftEditableTextField.resignFirstResponder()
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}

