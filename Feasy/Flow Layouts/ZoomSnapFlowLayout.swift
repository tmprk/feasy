//
//  ZoomSnapFlowLayout.swift
//  Feasy
//
//  Created by Xara on 11/30/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

class ZoomAndSnapFlowLayout: UICollectionViewFlowLayout, UICollectionViewDelegateFlowLayout {
    
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    let estimateWidth = UIScreen.main.bounds.width * 0.95
    //    let cellMarginSize = UIScreen.main.bounds.width / 20.5
    
    //    let estimateWidth = 160.0
    let cellMarginSize = 16.0
    
    override init() {
        super.init()
        setupGridView()
        scrollDirection = .vertical
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        guard let collectionView = collectionView else { fatalError() }
        let cellWidth: CGFloat = self.calculateWidth()
        itemSize = CGSize(width: cellWidth, height: cellWidth / 3)
        let numberOfCells = floor(collectionView.frame.size.width / cellWidth)
        let edgeInsets = (collectionView.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        sectionInset = UIEdgeInsets(top: edgeInsets, left: edgeInsets, bottom: edgeInsets, right: edgeInsets)
        super.prepare()
    }
    
    func calculateWidth() -> CGFloat {
        guard let collectionView = collectionView else { fatalError() }
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(collectionView.frame.size.width / estimatedWidth))
        let margin = CGFloat(cellMarginSize * 2)
        let width = (collectionView.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        return width
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        // Invalidate layout so that every cell get a chance to be zoomed when it reaches the center of the screen
        return true
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
    func setupGridView() {
        self.minimumLineSpacing = CGFloat(self.cellMarginSize)
        self.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        self.minimumInteritemSpacing = 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let statusBarOrientation = UIApplication.shared.statusBarOrientation
        // it is important to do this after presentModalViewController:animated:
        if (statusBarOrientation != UIInterfaceOrientation.portrait
            && statusBarOrientation != UIInterfaceOrientation.portraitUpsideDown){
            screenHeight = UIScreen.main.bounds.size.width
        } else {
            screenHeight = UIScreen.main.bounds.size.height
        }
        let width = self.calculateWidth()
        //        return CGSize(width: screenWidth * 0.436, height: screenHeight * 0.3)
        return CGSize(width: width, height: width / 3.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        //        let cellWidth: CGFloat = screenWidth * 0.436
        let cellWidth: CGFloat = self.calculateWidth()
        let numberOfCells = floor(collectionView.frame.size.width / cellWidth)
        let edgeInsets = (collectionView.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        return UIEdgeInsets(top: edgeInsets, left: edgeInsets, bottom: edgeInsets, right: edgeInsets)
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        // cannot exceed the edgeInsets number above or else changes rows
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //        return screenHeight * 0.015
        return CGFloat(cellMarginSize)
    }
    
    private func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}
