//
//  DSSCollectionViewFlowLayout.swift
//  Feasy
//
//  Created by Xara on 11/26/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

@objc(DSSCollectionViewFlowLayout)

class DSSCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    var insertingIndexPaths = [IndexPath]()
    
    override func prepare() {
        super.prepare()
        
        var contentByItems: ldiv_t
        
        let contentSize = self.collectionViewContentSize
        let itemSize = self.itemSize
        
        if UICollectionView.ScrollDirection.vertical == self.scrollDirection {
            contentByItems = ldiv (Int(contentSize.width), Int(itemSize.width))
        } else {
            contentByItems = ldiv (Int(contentSize.height), Int(itemSize.height))
        }
        
        let layoutSpacingValue = CGFloat(NSInteger (CGFloat(contentByItems.rem))) / CGFloat (contentByItems.quot + 1)
        
        let originalMinimumLineSpacing = self.minimumLineSpacing
        let originalMinimumInteritemSpacing = self.minimumInteritemSpacing
        let originalSectionInset = self.sectionInset
        
        if layoutSpacingValue != originalMinimumLineSpacing ||
            layoutSpacingValue != originalMinimumInteritemSpacing ||
            layoutSpacingValue != originalSectionInset.left ||
            layoutSpacingValue != originalSectionInset.right ||
            layoutSpacingValue != originalSectionInset.top ||
            layoutSpacingValue != originalSectionInset.bottom {
            
            let insetsForItem = UIEdgeInsets.init(top: layoutSpacingValue, left: layoutSpacingValue, bottom: layoutSpacingValue, right: layoutSpacingValue)
            
            self.minimumLineSpacing = layoutSpacingValue
            self.minimumInteritemSpacing = layoutSpacingValue
            self.sectionInset = insetsForItem
        }
    }
}

extension DSSCollectionViewFlowLayout {
    
    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)
        insertingIndexPaths.removeAll()
        for update in updateItems {
            if let indexPath = update.indexPathAfterUpdate,
                update.updateAction == .insert {
                insertingIndexPaths.append(indexPath)
            }
        }
        
    }
    
    override func finalizeCollectionViewUpdates() {
        super.finalizeCollectionViewUpdates()
        insertingIndexPaths.removeAll()
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
        if insertingIndexPaths.contains(itemIndexPath) {
//            attributes?.transform = CGAffineTransform(
//                translationX: 0,
//                y: 0
//            )
            attributes?.alpha = 0.0
            attributes?.transform = CGAffineTransform(
                scaleX: 0.1,
                y: 0.1
            )
        }
        return attributes
    }
}
