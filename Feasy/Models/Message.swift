//
//  Message.swift
//  Feasy
//
//  Created by Timothy Park on 12/13/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

class Message: NSObject {
    var title: String?
    var content: String?
    var toID: String?
    var time: TimeInterval?
    
    init(dictionary: [String: Any]) {
        super.init()
        title = dictionary["title"] as? String
        content = dictionary["content"] as? String
        toID = dictionary["toID"] as? String
        time = dictionary["time"] as? TimeInterval
    }
    
}
