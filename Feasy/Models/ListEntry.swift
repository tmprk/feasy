//
//  ListEntry.swift
//  Feasy
//
//  Created by Xara on 12/2/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

protocol DocumentSerializable {
    init?(dictionary:[String:Any])
}

struct ListEntry {
    
    var name: String
    var content: String
    var timestamp: Date
    
    var dictionary:[String:Any] {
        return [
            "name":name,
            "content":content,
            "timestamp":timestamp
        ]
    }
}

extension ListEntry: DocumentSerializable {
    
    init?(dictionary : [String : Any]) {
        guard let name = dictionary["name"] as? String,
            let content = dictionary["content"] as? String,
            let timestamp = dictionary["timestamp"] as? Date else {
                return nil
        }
        self.init(name: name, content: content, timestamp: timestamp)
    }
}
