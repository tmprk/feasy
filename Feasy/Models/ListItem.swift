//
//  CustomDataSource.swift
//  Feasy
//
//  Created by Xara on 12/6/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

class ListItem: NSObject {
    var title: String
    var content: String
    var isExpanded: Bool
    
    init(title:String, content: String, isExpanded: Bool){
        self.title = title
        self.content = content
        self.isExpanded = isExpanded
    }
}
