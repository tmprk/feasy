//
//  GlobalItem.swift
//  Feasy
//
//  Created by Timothy Park on 12/13/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

class GlobalItem: NSObject {
    var content: String
    var time: TimeInterval
    var username: String
    
    init(content:String, time: TimeInterval, username: String){
        self.content = content
        self.time = time
        self.username = username
    }
}
