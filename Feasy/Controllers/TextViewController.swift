//
//  TextViewController.swift
//  Feasy
//
//  Created by Xara on 11/25/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
class TextViewController: UIViewController, UITextViewDelegate {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    let laText: UITextView = {
        let TV = UITextView()
        TV.translatesAutoresizingMaskIntoConstraints = false
        TV.layer.masksToBounds = true
        guard let customFont = UIFont(name: "InterUI-Bold", size: 40) else {
            fatalError("Failed to load the font.")
        }
        TV.font = UIFontMetrics.default.scaledFont(for: customFont)
        TV.adjustsFontForContentSizeCategory = true
        TV.showsVerticalScrollIndicator = false
        TV.backgroundColor = .clear
        return TV
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setupStuff()
    }
    
    func setupStuff() {
        view.addSubview(laText)
        laText.delegate = self
        let textConstraints = [
            laText.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: screenHeight / 60),
            laText.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: screenHeight / 60),
            laText.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -screenHeight / 60),
            laText.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -screenHeight / 55)
        ]
        NSLayoutConstraint.activate(textConstraints)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
