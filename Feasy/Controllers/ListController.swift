//
//  ListController.swift
//  Feasy
//
//  Created by Timothy Park on 11/23/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

final class ListController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AddItem {
    
    var mainDataSourceNew: [Message] = [
//        Message(dictionary: ["title": "hello", "content": "hello what, once upon a time hello what, once upon a time hello what, once upon a time hello what, once upon a time bye bye", "username": "asashdfkxl2123", "time": 1432233446145.0]),
//        Message(dictionary: ["title": "hello", "content": "But unfortunately I haven't found good example. Do you know something about it? Maybe﻿?", "username": "asashdfkxl2123", "time": 1432233446145.0])
    ]
    
    var ref: DatabaseReference!
    let locationManager = CLLocationManager()
    var timer: Timer?
    
    var isExpanded = [Bool]()
    var list: [String] = ["data 0", "data 1", "data 2"]
    
    let cellId = "customCell"
    let flowLayout = DSSCollectionViewFlowLayout()
    
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    fileprivate var snapshotView: UIView?
    fileprivate var snapshotIndexPath: IndexPath?
    fileprivate var snapshotPanPoint: CGPoint?
    
    let estimateWidth = UIScreen.main.bounds.width * 0.95
    //    let cellMarginSize = UIScreen.main.bounds.width / 20.5
    
    //    let estimateWidth = 160.0
    let cellMarginSize = 16.0
    
    var cellWidth:CGFloat{
        return listCollectionView.frame.size.width
    }
    
    var expandedHeight : CGFloat = 200
    var notExpandedHeight : CGFloat = 50
    
    lazy var listCollectionView: UICollectionView = {
        let CV = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
        CV.backgroundColor = .clear
        CV.translatesAutoresizingMaskIntoConstraints = false
        CV.register(CustomCell.self, forCellWithReuseIdentifier: cellId)
        CV.alwaysBounceVertical = true
        CV.showsVerticalScrollIndicator = false
        CV.decelerationRate = .fast
        CV.dataSource = self
        CV.delegate = self
        return CV
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        generalSetup()
        setupConstraints()
        setupGridView()
        initLocation()
        observeMessages()
//        isExpanded = Array(repeating: false, count: list.count)
        
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.reloadTimes), userInfo: nil, repeats: true)
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressRecognized(_:)))
        gestureRecognizer.minimumPressDuration = 0.3
        listCollectionView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareNavigation(title: "List", font: UIFont(name: "Avenir-Medium", size: 24) ?? UIFont.systemFont(ofSize: 23, weight: .medium))
    }
}

extension ListController {
    
    func getId(handler: @escaping (String) -> Void){
        let uid = Auth.auth().currentUser?.uid
        self.ref.child("users").child(uid!).observe(.value, with: {(snapshot) in
            if let userDictionary = snapshot.value as? [String: AnyObject] {
                if let username = userDictionary["username"] as? String {
                    handler(username)
                }
            }
        }, withCancel: nil)
    }
    
    func observeMessages() {
        self.ref.child("Messages").observe(.childAdded, with: {(snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject] {
                let newListItem = Message(dictionary: dictionary)
                self.getId(handler: {(username) in
                    if username == newListItem.toID {
                        self.mainDataSourceNew.insert(newListItem, at: 0)
                        self.listCollectionView.performBatchUpdates({
                            self.listCollectionView.insertItems(at: [IndexPath(item: 0, section: 0)])
                        }, completion: nil)
                        DispatchQueue.main.async {
                            self.listCollectionView.reloadData()
                            print(self.mainDataSourceNew.count)
                        }
                    }
                })
            }
            print(snapshot)
        }, withCancel: nil)
    }
    
    @objc func reloadTimes() {
        listCollectionView.reloadItems(at: listCollectionView.indexPathsForVisibleItems)
    }
    
}

extension ListController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let statusBarOrientation = UIApplication.shared.statusBarOrientation
        // it is important to do this after presentModalViewController:animated:
        if (statusBarOrientation != UIInterfaceOrientation.portrait
            && statusBarOrientation != UIInterfaceOrientation.portraitUpsideDown){
            screenHeight = UIScreen.main.bounds.size.width
        } else {
            screenHeight = UIScreen.main.bounds.size.height
        }
        let width = self.calculateWidth()
        //        return CGSize(width: screenWidth * 0.436, height: screenHeight * 0.3)
        
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Light", size: 15) ?? UIFont.systemFont(ofSize: 15)]
        let estimatedFrame = NSString(string: mainDataSourceNew[indexPath.item].content!).boundingRect(with: CGSize(width: width - 36, height: 10000), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        
        return CGSize(width: width, height: estimatedFrame.height + 63)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //        let cellWidth: CGFloat = screenWidth * 0.436
        let cellWidth: CGFloat = self.calculateWidth()
        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        return UIEdgeInsets(top: edgeInsets, left: edgeInsets, bottom: edgeInsets, right: edgeInsets)
    }
    
    func calculateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        return width
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        // cannot exceed the edgeInsets number above or else changes rows
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //        return screenHeight * 0.015
        return CGFloat(cellMarginSize)
    }
    
    func setupGridView() {
        let flow = listCollectionView.collectionViewLayout as! DSSCollectionViewFlowLayout
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumInteritemSpacing = 0
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("starting index: \(sourceIndexPath.item)")
        print("destination index: \(destinationIndexPath.item)")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = listCollectionView.collectionViewLayout as? DSSCollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (mainDataSourceNew.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomCell
        cell.indexPath = indexPath
        cell.deleteDelegate = self
        cell.contentView.backgroundColor = UIColor(red:0.20, green:0.62, blue:0.60, alpha:1.0)
        
        if let timeInterval = mainDataSourceNew[indexPath.item].time {
            let time = Date(timeIntervalSince1970: timeInterval)
            cell.timeLabel.text = time.timeAgoDisplay()
        }

//        cell.timeLabel.text = mainDataSourceNew[indexPath.item].time
        cell.contentLabel.text = mainDataSourceNew[indexPath.item].content
        return cell
    }
    
    func generalSetup() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "newMessage"), style: .plain, target: self, action: #selector(gotoVCB(_:)))
    }
    
    func setupConstraints() {
        view.backgroundColor = .white
        view.addSubview(listCollectionView)
        
        let cardViewConstraints = [
            listCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            listCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            listCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            listCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(cardViewConstraints)
    }
    
    @objc func gotoVCB(_ sender: UIButton) {
        let vc = ComposeController()
        vc.delegate = self
        vc.locationDelegate = self
        vc.modalPresentationStyle = .custom
        present(vc, animated: true, completion: nil)
    }

}

extension ListController: UIGestureRecognizerDelegate {
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        mainDataSourceNew.remove(at: indexPath.item)
        collectionView.deleteItems(at: [indexPath])
    }
    
    @objc func longPressRecognized(_ recognizer: UILongPressGestureRecognizer) {
        let location = recognizer.location(in: listCollectionView)
        let indexPath = listCollectionView.indexPathForItem(at: location)
        
        switch recognizer.state {
        case UIGestureRecognizer.State.began:
            guard let indexPath = indexPath else { return }
//            print(isExpanded[indexPath.row])
            let cell = cellForRow(at: indexPath)
            snapshotView = cell.snapshotView(afterScreenUpdates: true)
            listCollectionView.addSubview(snapshotView!)
            cell.contentView.alpha = 0.0
            
            UIView.animate(withDuration: 0.2, animations: {
                self.snapshotView?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                self.snapshotView?.alpha = 1.0
            })
            snapshotPanPoint = location
            snapshotIndexPath = indexPath
        case UIGestureRecognizer.State.changed:
            guard let snapshotPanPoint = snapshotPanPoint else { return }
            
            let translation = CGPoint(x: location.x - snapshotPanPoint.x, y: location.y - snapshotPanPoint.y)
            snapshotView?.center.y += translation.y
            self.snapshotPanPoint = location
            
            guard let indexPath = indexPath else { return }
            print(indexPath.item)
            
            let src0 = mainDataSourceNew[snapshotIndexPath!.item]
            mainDataSourceNew.remove(at: snapshotIndexPath!.row)
            mainDataSourceNew.insert(src0, at: indexPath.item)

            listCollectionView.moveItem(at: snapshotIndexPath!, to: indexPath)

            snapshotIndexPath = indexPath
            listCollectionView.reloadData()
        default:
            guard let snapshotIndexPath = snapshotIndexPath else { return }
            let cell = cellForRow(at: snapshotIndexPath)
            UIView.animate(
                withDuration: 0.2,
                animations: {
                    self.snapshotView?.center = cell.center
                    self.snapshotView?.transform = CGAffineTransform.identity
                    self.snapshotView?.alpha = 1.0
            },
                completion: { finished in
                    cell.contentView.alpha = 1.0
                    self.snapshotView?.removeFromSuperview()
                    self.snapshotView = nil
                    self.listCollectionView.reloadData()
            })
            self.snapshotIndexPath = nil
            self.snapshotPanPoint = nil
        }
    }
}

extension ListController {
    
    fileprivate func cellForRow(at indexPath: IndexPath) -> CustomCell {
        return listCollectionView.cellForItem(at: indexPath) as! CustomCell
    }
    
}

extension ListController {
    
    func sendData(titleString: String, contentString: String) {
        print("hello")
//        mainDataSource.append(ListItem(title: titleString, content: contentString, isExpanded: false))
//        let indexPath = IndexPath(
//            item: self.mainDataSourceNew.count - 1,
//            section: 0
//        )
//        listCollectionView.performBatchUpdates({
//            self.listCollectionView.insertItems(at: [indexPath])
//        }, completion: nil)
//        listCollectionView.reloadData()
    }
}

extension ListController: DeleteFunction {
    func cellDelete(indexPath: IndexPath) {
        self.listCollectionView.performBatchUpdates({
            mainDataSourceNew.remove(at: indexPath.item)
            listCollectionView.deleteItems(at: [indexPath])
            // delete cell from database
        }) { (finished) in
            self.listCollectionView.reloadItems(at: self.listCollectionView.indexPathsForVisibleItems)
            print("Item at \(indexPath.item) deleted")
        }
        print("\(mainDataSourceNew.count) items left")
    }
}

extension ListController: CLLocationManagerDelegate {
    
    func initLocation() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            startLocation()
            break
        case .restricted, .denied:
            print("show popup of how to turn it on")
            redirectToPrivacy()
            break
        case .authorizedAlways:
            print("authorization always success")
//            startLocation()
            break
        case .authorizedWhenInUse:
            print("authorization to when in use success")
            break
        }
    }
    
    func escalateLocationServiceAuthorization() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func startLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    @objc func redirectToPrivacy() {
        let alert = UIAlertController(title: "Allow access Always", message: "This is my message.", preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func reverseGeocode() {
        guard let locValue: CLLocationCoordinate2D = locationManager.location?.coordinate else { return }
        geocode(latitude: locValue.latitude, longitude: locValue.longitude, completion: { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            // you should always update your UI in the main thread
            DispatchQueue.main.async {
//                  update UI here
                    print("address1:", placemark.thoroughfare ?? "")
                    print("address2:", placemark.subThoroughfare ?? "")
                    print("city:",     placemark.locality ?? "")
                    print("state:",    placemark.administrativeArea ?? "")
                    print("zip code:", placemark.postalCode ?? "")
                    print("country:",  placemark.country ?? "")
            }
        })
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    func getAddressFromLatLon(latitude: Double, longitude: Double) {
        let ceo: CLGeocoder = CLGeocoder()
        let loc: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
        print(loc)
        
        ceo.reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) in
            if (error != nil) {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            let pm = placemarks! as [CLPlacemark]
            if pm.count > 0 {
                let pm = placemarks![0]
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                print(addressString)
            }
        })
    }
}

extension ListController: populateDistanceFrom {
    
    func showString(handler: @escaping (String) -> Void) {
        var resultString: String = ""
        guard let locValue: CLLocationCoordinate2D = locationManager.location?.coordinate else { return }
        geocode(latitude: locValue.latitude, longitude: locValue.longitude, completion: {(placemark, error) in
            if (error != nil) {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            guard let placemark = placemark, error == nil else { return }
            if let result = placemark.subAdministrativeArea {
                resultString = result
            }
            handler(resultString)
        })
    }
}

