//
//  EntryController.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase

class EntryController: UIViewController {
    
    var ref: DatabaseReference!
    
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    let signInButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.layer.borderColor = UIColor.white.cgColor
        button.adjustsImageWhenHighlighted = false
        button.layer.borderWidth = 4.0
        button.layer.cornerRadius = 4.0
        button.backgroundColor = UIColor.clear
        button.setTitle("Sign In", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    
    let signUpButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.layer.borderWidth = 4.0
        button.layer.borderColor = UIColor.white.cgColor
        button.adjustsImageWhenHighlighted = false
        button.layer.cornerRadius = 4.0
        button.backgroundColor = UIColor.white
        button.setTitle("Sign Up", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(UIColor(red:0.27, green:0.46, blue:0.83, alpha:1.0), for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        snowRandomShit()
        self.navigationController?.isNavigationBarHidden = true
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
        self.view.backgroundColor = UIColor(red:0.26, green:0.37, blue:0.76, alpha:1.0)
        
        setupButtons()
        signInButton.addTarget(self, action: #selector(setupLoginView), for: .touchUpInside)
        signUpButton.addTarget(self, action: #selector(setupSignUpView), for: .touchUpInside)
    }
}

extension EntryController {
    
    @objc func setupSignUpView() {
        let modalVC = SignUpController()
        modalVC.modalTransitionStyle = .crossDissolve
        modalVC.modalPresentationStyle = .overCurrentContext
        self.present(modalVC, animated: true, completion: nil)
    }
    
    @objc func setupLoginView() {
        let modalVC = SignInController()
        modalVC.modalTransitionStyle = .crossDissolve
        modalVC.modalPresentationStyle = .overCurrentContext
        self.present(modalVC, animated: true, completion: nil)
    }
    
    private func setupButtons() {
        view.addSubview(signInButton)
        view.addSubview(signUpButton)
        
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        
        let widthContraintsSI = NSLayoutConstraint(item: signInButton, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 0.84, constant: 0)
        let xConstraintsSI = NSLayoutConstraint(item: signInButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        
        let widthContraintsSU = NSLayoutConstraint(item: signUpButton, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 0.84, constant: 0)
        let xConstraintsSU = NSLayoutConstraint(item: signUpButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        
        var buttonHeight: CGFloat?
        var buttonPaddingSI: CGFloat?
        var buttonPaddingSU: CGFloat?
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                buttonHeight = screenHeight / 10
                buttonPaddingSI = screenHeight / 50
                buttonPaddingSU = screenHeight / 22
                signInButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20, weight: .bold)
                signUpButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20, weight: .bold)
            case 1334:
                buttonHeight = screenHeight / 9
                buttonPaddingSI = screenHeight / 58
                buttonPaddingSU = 25
                signInButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 23) ?? UIFont.systemFont(ofSize: 23, weight: .bold)
                signUpButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 23) ?? UIFont.systemFont(ofSize: 23, weight: .bold)
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                buttonHeight = screenHeight / 9.5
                buttonPaddingSI = screenHeight / 60
                buttonPaddingSU = screenHeight / 50
            case 2436:
                print("iPhone X, Xs")
                buttonHeight = screenHeight / 11.5
                buttonPaddingSI = screenHeight / 55
                buttonPaddingSU = screenHeight / 30
                signInButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 25) ?? .systemFont(ofSize: 25, weight: .semibold)
                signUpButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 25) ?? .systemFont(ofSize: 25, weight: .semibold)
            case 2688:
                print("iPhone Xs Max")
                buttonHeight = screenHeight / 11.5
                buttonPaddingSI = screenHeight / 55
                buttonPaddingSU = screenHeight / 30
            case 1792:
                print("iPhone Xr")
                buttonHeight = screenHeight / 11.5
                buttonPaddingSI = screenHeight / 55
                buttonPaddingSU = screenHeight / 30
            default:
                print("unknown")
            }
        }
        
        let heightConstraintsSI = NSLayoutConstraint(item: signInButton, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 0, constant: buttonHeight!)
        let yConstraintsSI = NSLayoutConstraint(item: signInButton, attribute: .bottom, relatedBy: .equal, toItem: signUpButton, attribute: .top, multiplier: 1, constant: -buttonPaddingSI!)
        let heightConstraintsSU = NSLayoutConstraint(item: signUpButton, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 0, constant: buttonHeight!)
        let yConstraintsSU = NSLayoutConstraint(item: signUpButton, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -buttonPaddingSU!)
        
        NSLayoutConstraint.activate([widthContraintsSI, xConstraintsSI,heightConstraintsSI, yConstraintsSI])
        NSLayoutConstraint.activate([widthContraintsSU, xConstraintsSU, heightConstraintsSU, yConstraintsSU])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.report_memory()
    }
}

extension UIViewController {
    func snowRandomShit() {
        let emitter = Emitter.get(with: UIImage(named: "confetti")!)
        emitter.position = CGPoint(x: view.frame.width / 2, y: 0)
        emitter.emitterSize = CGSize(width: view.frame.width, height: 1)
        view.layer.addSublayer(emitter)
    }
    
    func endRandomShit() {
        view.layer.removeAllAnimations()
        view.layer.removeFromSuperlayer()
    }
}

extension EntryController {
    func report_memory() {
        var info = mach_task_basic_info()
        var count = mach_msg_type_number_t(MemoryLayout<mach_task_basic_info>.size)/4
        
        let kerr: kern_return_t = withUnsafeMutablePointer(to: &info) {
            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
                task_info(mach_task_self_,
                          task_flavor_t(MACH_TASK_BASIC_INFO),
                          $0,
                          &count)
            }
        }
        if kerr == KERN_SUCCESS {
            print("Memory in use (in bytes): \(info.resident_size)")
        }
        else {
            print("Error with task_info(): " +
                (String(cString: mach_error_string(kerr), encoding: String.Encoding.ascii) ?? "unknown error"))
        }
    }
}
