//
//  ComposeController.swift
//  Feasy
//
//  Created by Xara on 12/2/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase

final class ComposeController: UIViewController, UITextViewDelegate {
    
    var ref: DatabaseReference!
    
    var delegate: AddItem?
    var locationDelegate: populateDistanceFrom?
    var isPresenting = false
    let screenWidth = UIScreen.main.bounds.width
    
    lazy var backdropView: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return bdView
    }()
    
    lazy var menuView: ComposeView = {
        let menuView = ComposeView()
        menuView.backgroundColor = UIColor.white
        menuView.translatesAutoresizingMaskIntoConstraints = false
        return menuView
    }()
    
    lazy var sampleText: UITextView = {
        let sp = UITextView()
        sp.delegate = self
        sp.tintColor = UIColor(red:0.27, green:0.46, blue:0.83, alpha:1.0)
        sp.translatesAutoresizingMaskIntoConstraints = false
        sp.font = UIFont.systemFont(ofSize: 18)
        sp.isScrollEnabled = false
        sp.sizeToFit()
        return sp
    }()
    
    let floatingButton: UIButton = {
        let fb = UIButton(type: .system)
        fb.translatesAutoresizingMaskIntoConstraints = false
        fb.setTitleColor(.white, for: .normal)
        fb.setTitle("Done", for: .normal)
        fb.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        fb.backgroundColor = UIColor(red:0.36, green:0.69, blue:0.55, alpha:1.0)
        fb.layer.cornerRadius = 5
        fb.titleLabel?.adjustsFontSizeToFitWidth = true
        fb.titleLabel?.minimumScaleFactor = 0.4
        fb.alpha = 0.0
        fb.addTarget(self, action: #selector(handleData), for: .touchUpInside)
        return fb
    }()
    
    let navItem = UINavigationItem()
    let navBar: UINavigationBar = {
        let nb = UINavigationBar()
        nb.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .bold)
        ]
        nb.translatesAutoresizingMaskIntoConstraints = false
        return nb
    }()
    
    let leftEditableTextField: UITextField = {
        let leftEditableTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 250, height: 30))
//        middleEditableText.textAlignment = .center
        leftEditableTextField.textColor = UIColor(red:0.18, green:0.24, blue:0.47, alpha:1.0)
        leftEditableTextField.tintColor = UIColor(red:0.18, green:0.24, blue:0.47, alpha:1.0)
        leftEditableTextField.placeholder = "Username"
        leftEditableTextField.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        return leftEditableTextField
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        view.backgroundColor = .clear
        view.addSubview(backdropView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        formatStringInMenuView()
        setupMenuView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ComposeController.handleTap(_:)))
        backdropView.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.27, green:0.46, blue:0.83, alpha:1.0)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self.view.window)
    }
}

extension ComposeController {
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        sampleText.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    func setupMenuView() {
        view.addSubview(menuView)
        menuView.addSubview(navBar)
        menuView.addSubview(sampleText)
        view.addSubview(floatingButton)
//        menuView.frame.size.height = sampleText.frame.size.height
        
        let floatingButtonConstraints = [
            floatingButton.trailingAnchor.constraint(equalTo: menuView.trailingAnchor),
            floatingButton.bottomAnchor.constraint(equalTo: menuView.topAnchor, constant: -(UIScreen.main.bounds.width * 0.05) / 2),
            floatingButton.widthAnchor.constraint(equalToConstant: calculateWidth() / 5)
        ]
        
        let labelInset = UIEdgeInsets(top: 10, left: 10, bottom: -10, right: -10)
        let menuViewContraints = [
            menuView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            menuView.widthAnchor.constraint(equalToConstant: calculateWidth()),
//            menuView.heightAnchor.constraint(equalToConstant: 95),
            menuView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -(screenWidth - calculateWidth()) / 2),
            
            navBar.topAnchor.constraint(equalTo: menuView.topAnchor),
            navBar.leadingAnchor.constraint(equalTo: menuView.leadingAnchor),
            navBar.trailingAnchor.constraint(equalTo: menuView.trailingAnchor),
            
            sampleText.topAnchor.constraint(equalTo: navBar.bottomAnchor, constant: labelInset.top),
            sampleText.bottomAnchor.constraint(equalTo: menuView.bottomAnchor, constant: labelInset.bottom),
            sampleText.leadingAnchor.constraint(equalTo: menuView.leadingAnchor, constant: labelInset.left),
            sampleText.trailingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: labelInset.right),
//            sampleText.heightAnchor.constraint(equalToConstant: 35),
        ]
        NSLayoutConstraint.activate(menuViewContraints)
        NSLayoutConstraint.activate(floatingButtonConstraints)
    }
    
    func formatStringInMenuView() {
        let sendButton = UIButton(type: .system)
        sendButton.tintColor = UIColor(red:0.18, green:0.24, blue:0.47, alpha:1.0)
        sendButton.setImage(UIImage(named: "sendButton"), for: .normal)
        sendButton.addTarget(self, action: #selector(handleData), for: .touchUpInside)
        let rightMostButton = UIBarButtonItem(customView: sendButton)
        rightMostButton.isEnabled = false
        let leftMostButton = UIBarButtonItem(customView: leftEditableTextField)
        
        navItem.rightBarButtonItem = rightMostButton
        navItem.leftBarButtonItem = leftMostButton
        navBar.setItems([navItem], animated: true)
//        self.navItem.titleView = middleEditableTextField
        
        self.locationDelegate?.showString(handler: {(placemark) in
//            self.navItem.title = "\(placemark)📍"
        })
    }
    
    @objc func handleData() {
        guard let toId = leftEditableTextField.text else { return }
        guard let content = sampleText.text else { return }
        let timestamp = NSDate().timeIntervalSince1970
        
        let usersReference = self.ref.child("Messages")
        let childref = usersReference.childByAutoId()
        let values = ["toID": toId, "content": content, "time": timestamp] as [String : Any]
        
        childref.updateChildValues(values, withCompletionBlock: { (error: Error?, ref: DatabaseReference) in
            if error != nil {
                print("\(error!.localizedDescription)")
            } else {
                print("Data saved successfully!")
//                self.delegate?.sendData(titleString: toId, contentString: content)
                self.sampleText.text = ""
                self.sampleText.endEditing(true)
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func calculateWidth() -> CGFloat {
        let cellCount = floor(CGFloat(self.view.frame.size.width / (screenWidth * 0.95)))
        let margin = CGFloat(16 * 2)
        let width = (self.view.frame.size.width - 16 * (cellCount - 1) - margin) / cellCount
        return width
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}

extension ComposeController {
    
    func textViewDidChange(_ textView: UITextView) {
        guard let navBarRight = navItem.rightBarButtonItem else { return }
        if !sampleText.text!.isEmpty && !leftEditableTextField.text!.isEmpty {
            navBarRight.isEnabled = true
        } else {
            navBarRight.isEnabled = false
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        let newLineCount = newText.filter {$0 == "\n"}.count
        if !(newLineCount <= 10) {
            return false
        }
        return numberOfChars <= 200 // 200 Character Limit Value
    }
    
}
