//
//  ViewController.swift
//  Feasy
//
//  Created by Timothy Park on 11/21/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase

class TabBarController: UITabBarController {
    
    var ref: DatabaseReference!
    var handle: AuthStateDidChangeListenerHandle?
    
    enum tabBarMenu: Int {
        case Chat
        case List
        case Profile
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        tabBarSetup()
        generalSetup()
        handleAuthSession()
        checkLogin()
        createGradient()
        self.selectedViewController = self.viewControllers?[1]
    }
}

extension TabBarController {
    
    func generalSetup() {
        let chatViewController = ChatController()
        chatViewController.view.backgroundColor = UIColor(red:0.55, green:0.89, blue:0.88, alpha:1.0)
        
        let listViewController = ListController()
        listViewController.view.backgroundColor = UIColor(red:0.55, green:0.89, blue:0.88, alpha:1.0)

        let profileViewController = ProfileController()
        profileViewController.view.backgroundColor = UIColor(red:0.55, green:0.89, blue:0.88, alpha:1.0)

        let viewControllerList = [
            makeNavController(view: chatViewController, imageName: "largeChatIcon", barTint: UIColor.white),
            makeNavController(view: listViewController, imageName: "largeChatIcon", barTint: UIColor.white),
            makeNavController(view: profileViewController, imageName: "largeProfileIcon", barTint: UIColor.white)
        ]
        
        viewControllers = viewControllerList.map { $0 }
        guard let tabBarMenuItem = tabBarMenu(rawValue: 0) else { return }
        setTintColor(forMenuItem: tabBarMenuItem)
    }
}

extension TabBarController {
    
    private func makeNavController(view: UIViewController, imageName: String, barTint: UIColor) -> UINavigationController {
        let viewController = view
        let navController = UINavigationController(rootViewController: viewController)
        navController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: barTint, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .bold)]
        navController.tabBarItem.image = UIImage(named: imageName)
        navController.navigationBar.tintColor = barTint
        navController.navigationBar.isTranslucent = false
        return navController
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard
            let menuItemSelected = tabBar.items?.index(of: item),
            let tabBarMenuItem = tabBarMenu(rawValue: menuItemSelected)
            else { return }
        setTintColor(forMenuItem: tabBarMenuItem)
    }
    
    private func setTintColor(forMenuItem tabBarMenuItem: tabBarMenu) {
        switch tabBarMenuItem {
        case .Chat:
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = .white
        case .List:
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = .white
        case .Profile:
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = .white
        }
    }

}

extension TabBarController {
    
    func createGradient() {
        let layerGradient = CAGradientLayer()
        layerGradient.startPoint = CGPoint(x: 0.5, y: 1.0)
        layerGradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        layerGradient.colors = [UIColor(white: 0.0, alpha: 0.6).cgColor, UIColor(white: 0.0, alpha: 0.45).cgColor, UIColor(white: 0.0, alpha: 0.25).cgColor, UIColor(white: 0.0, alpha: 0.1).cgColor, UIColor(white: 0.0, alpha: 0.0).cgColor]
        layerGradient.frame = CGRect(x: 0, y: self.tabBar.bounds.height, width: self.tabBar.bounds.width, height: -(2 * self.tabBar.bounds.height))
        self.tabBar.layer.insertSublayer(layerGradient, at: 0)
    }
    
    func tabBarSetup() {
        self.tabBar.unselectedItemTintColor = UIColor(red:0.56, green:0.96, blue:0.93, alpha:1.0)
//        self.tabBar.unselectedItemTintColor = UIColor(red:0.58, green:0.96, blue:0.89, alpha:1.0) pretty good
//        self.tabBar.unselectedItemTintColor = UIColor(red:0.20, green:0.62, blue:0.60, alpha:1.0)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .semibold)], for: .normal)
    }
}

extension TabBarController {
    
    override func viewDidDisappear(_ animated: Bool) {
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    func checkLogin() {
        if Auth.auth().currentUser?.uid != nil {
            return
        } else {
            handleLogout()
        }
    }
    
    func handleLogout() {
        do {
            try Auth.auth().signOut()
            self.present(EntryController(), animated: false, completion: nil)
        } catch let error as NSError {
            print ("\(error.localizedDescription)")
        }
    }
    
    func handleAuthSession() {
        handle = Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                return
            } else {
                self.present(EntryController(), animated: false, completion: nil)
            }
        }
    }
}
