//
//  SignInController.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase

class SignInController: UIViewController, UITextFieldDelegate {
    
    var ref: DatabaseReference!
    
    var shouldSetupConstraints = true
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    let usernameText: UILabel = {
        let usernameText = UILabel()
        usernameText.text = "Email"
        usernameText.textColor = UIColor.white
        return usernameText
    }()
    
    let emailBox: UITextField = {
        let emailBox = UITextField()
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        emailBox.leftView = paddingView
        emailBox.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:0.3)
        emailBox.leftViewMode = .always
        emailBox.layer.borderColor = UIColor.white.cgColor
        emailBox.textColor = UIColor.white
        emailBox.tintColor = UIColor.clear
        emailBox.textAlignment = .left
        emailBox.layer.cornerRadius = 2.8
        emailBox.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [.foregroundColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.8), .font: UIFont(name: "Avenir-Medium", size: 17) ?? UIFont.systemFont(ofSize: 17)])
        emailBox.layer.borderWidth = 2.3
        return emailBox
    }()
    
    let passwordText: UILabel = {
        let passwordText = UILabel()
        passwordText.text = "Password"
        passwordText.textColor = UIColor.white
        return passwordText
    }()
    
    let passwordBox: UITextField = {
        let passwordBox = UITextField()
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        passwordBox.isSecureTextEntry = true
        passwordBox.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:0.3)
        passwordBox.leftView = paddingView
        passwordBox.layer.borderColor = UIColor.white.cgColor
        passwordBox.layer.borderWidth = 2.3
        passwordBox.leftViewMode = .always
        passwordBox.textColor = UIColor.white
        passwordBox.tintColor = UIColor.clear
        passwordBox.textAlignment = .left
        passwordBox.layer.cornerRadius = 2.8
        passwordBox.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [.foregroundColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.8), .font: UIFont(name: "Avenir-Medium", size: 17) ?? UIFont.systemFont(ofSize: 17)])
        return passwordBox
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.layer.borderWidth = 3.0
        button.layer.borderColor = UIColor.white.cgColor
        button.backgroundColor = UIColor.white
        button.setTitle("Login", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(UIColor(red:0.27, green:0.46, blue:0.83, alpha:1.0), for: .normal)
        button.adjustsImageWhenHighlighted = false
        button.layer.cornerRadius = 3.4
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    let createAccountButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.adjustsImageWhenHighlighted = false
        button.isUserInteractionEnabled = true
        button.layer.cornerRadius = 5.0
        button.setTitle("Don't have an account?", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(UIColor.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let closeButton: UIButton = {
        let button = UIButton(type: .custom) as UIButton
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "closeButton")
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(singleTapping(recognizer:)), for: .touchUpInside)
        return button
    }()
}

extension SignInController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        self.view.backgroundColor = UIColor(red:0.26, green:0.37, blue:0.76, alpha:1.0)
        snowRandomShit()
        
        emailBox.delegate = self
        passwordBox.delegate = self
        
        emailBox.addTarget(self, action: #selector(addColorToBorder(textfield:)), for: .touchDown)
        passwordBox.addTarget(self, action: #selector(addColorToBorder(textfield:)), for: .touchDown)
        layoutViews()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailBox {
            textField.resignFirstResponder()
            emailBox.bringBackToNormal()
            passwordBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            passwordBox.becomeFirstResponder()
        } else if textField == passwordBox {
            textField.resignFirstResponder()
            emailBox.bringBackToNormal()
            passwordBox.bringBackToNormal()
        }
        return true
    }
    
    @objc func addColorToBorder(textfield: UITextField) {
        if textfield == emailBox {
            self.emailBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            self.passwordBox.bringBackToNormal()
        } else if textfield == passwordBox {
            self.passwordBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            self.emailBox.bringBackToNormal()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.emailBox.bringBackToNormal()
        self.passwordBox.bringBackToNormal()
        self.view.endEditing(true)
    }
    
    @objc func singleTapping(recognizer: UIGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didEndEditing() {
        for textField in self.view.subviews where textField is UITextField {
            textField.resignFirstResponder()
        }
    }
    
    func layoutViews() {
        self.view.addSubview(usernameText)
        self.view.addSubview(emailBox)
        self.view.addSubview(passwordText)
        self.view.addSubview(passwordBox)
        self.view.addSubview(closeButton)
        self.view.addSubview(loginButton)
        self.view.addSubview(createAccountButton)
        
        usernameText.translatesAutoresizingMaskIntoConstraints = false
        emailBox.translatesAutoresizingMaskIntoConstraints = false
        passwordText.translatesAutoresizingMaskIntoConstraints = false
        passwordBox.translatesAutoresizingMaskIntoConstraints = false
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        createAccountButton.translatesAutoresizingMaskIntoConstraints = false
        
        var margin: CGFloat?
        var heightPadding: CGFloat?
        var usernameTopPadding: CGFloat?
        var textBoxHeight: CGFloat?
        var textboxWidth: CGFloat?
        var betweenThings: CGFloat?
        var closeButtonMargins: CGFloat?
        var buttonWidths: CGFloat?
        var betweenButtonAndBox: CGFloat?
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                margin = screenWidth / 8
                usernameTopPadding = screenHeight / 4.8
                heightPadding = screenHeight / 60
                textboxWidth = screenWidth - (2 * margin!)
                textBoxHeight = screenHeight / 15
                usernameText.font = UIFont(name: "Avenir-Medium", size: 19) ?? UIFont.systemFont(ofSize: 19)
                emailBox.font = UIFont(name: "Avenir", size: 16) ?? UIFont.systemFont(ofSize: 16)
                passwordText.font = UIFont(name: "Avenir-Medium", size: 19) ?? UIFont.systemFont(ofSize: 19)
                passwordBox.font = UIFont(name: "Avenir", size: 16) ?? UIFont.systemFont(ofSize: 16)
                betweenThings = screenHeight / 40
                closeButtonMargins = (heightPadding! + 7)
                buttonWidths = self.view.frame.size.width / 2.2
                betweenButtonAndBox = self.view.frame.size.height / 13.5
                createAccountButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 14) ?? .systemFont(ofSize: 14, weight: .bold)
                loginButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 16.5) ?? .systemFont(ofSize: 16.5, weight: .semibold)
            case 1334:
                margin = screenWidth / 8
                usernameTopPadding = screenHeight / 4.8
                heightPadding = screenHeight / 60
                textboxWidth = screenWidth - (2 * margin!)
                textBoxHeight = screenHeight / 15
                usernameText.font = UIFont(name: "Avenir-Medium", size: 21) ?? UIFont.systemFont(ofSize: 21)
                emailBox.font = UIFont(name: "Avenir", size: 18) ?? UIFont.systemFont(ofSize: 18)
                passwordText.font = UIFont(name: "Avenir-Medium", size: 21) ?? UIFont.systemFont(ofSize: 21)
                passwordBox.font = UIFont(name: "Avenir", size: 18) ?? UIFont.systemFont(ofSize: 18)
                betweenThings = screenHeight / 40
                closeButtonMargins = (heightPadding! + 7)
                buttonWidths = self.view.frame.size.width / 2.2
                betweenButtonAndBox = self.view.frame.size.height / 13.5
                createAccountButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 16) ?? .systemFont(ofSize: 16, weight: .bold)
                loginButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20, weight: .bold)
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X, Xs")
                margin = screenWidth / 8.5
                usernameTopPadding = screenHeight / 4.4
                heightPadding = screenHeight / 90
                textboxWidth = screenWidth - (2 * margin!)
                textBoxHeight = screenHeight / 20
                usernameText.font = UIFont(name: "Avenir-Medium", size: 24) ?? .systemFont(ofSize: 24, weight: .bold)
                emailBox.font = UIFont(name: "Avenir-Black", size: 15) ?? .systemFont(ofSize: 15, weight: .bold)
                passwordText.font = UIFont(name: "Avenir-Medium", size: 24) ?? .systemFont(ofSize: 24, weight: .bold)
                passwordBox.font = UIFont(name: "Avenir", size: 15) ?? .systemFont(ofSize: 15, weight: .bold)
                betweenThings = screenHeight / 40
                closeButtonMargins = (heightPadding! + 7)
                buttonWidths = self.view.frame.size.width / 2
                betweenButtonAndBox = self.view.frame.size.height / 5.8
                createAccountButton.titleLabel?.font = UIFont(name: "Avenir", size: 16) ?? .systemFont(ofSize: 16, weight: .bold)
                loginButton.titleLabel?.font = UIFont(name: "Avenir-Black", size: 18) ?? .systemFont(ofSize: 18, weight: .semibold)
            case 2688:
                print("iPhone Xs Max")
            case 1792:
                print("iPhone Xr")
            default:
                print("unknown")
            }
        }
        
        let usernameTextBoxWidth = NSLayoutConstraint(item: emailBox, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: textboxWidth!)
        let usernameTextBoxX = NSLayoutConstraint(item: emailBox, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let usernameTextBoxY = NSLayoutConstraint(item: emailBox, attribute: .top, relatedBy: .equal, toItem: usernameText, attribute: .bottom, multiplier: 1, constant: heightPadding!)
        let usernameTextBoxHeight = NSLayoutConstraint(item: emailBox, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0, constant: textBoxHeight!)
        
        let usernameTextX = NSLayoutConstraint(item: usernameText, attribute: .leading, relatedBy: .equal, toItem: emailBox, attribute: .leading, multiplier: 1, constant: 0)
        let usernameTextY = NSLayoutConstraint(item: usernameText, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: usernameTopPadding!)
        
        let passwordTextBoxWidth = NSLayoutConstraint(item: passwordBox, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: textboxWidth!)
        let passwordTextBoxX = NSLayoutConstraint(item: passwordBox, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let passwordTextBoxY = NSLayoutConstraint(item: passwordBox, attribute: .top, relatedBy: .equal, toItem: passwordText, attribute: .bottom, multiplier: 1, constant: heightPadding!)
        let passwordTextBoxHeight = NSLayoutConstraint(item: passwordBox, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0, constant: textBoxHeight!)
        
        let passwordTextX = NSLayoutConstraint(item: passwordText, attribute: .leading, relatedBy: .equal, toItem: usernameText, attribute: .leading, multiplier: 1, constant: 0)
        let passwordTextY = NSLayoutConstraint(item: passwordText, attribute: .top, relatedBy: .equal, toItem: emailBox, attribute: .bottom, multiplier: 1, constant: betweenThings!)
        
        let closeButtonX = NSLayoutConstraint(item: closeButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: closeButtonMargins!)
        
        if #available(iOS 11, *) {
            let guide = view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                closeButton.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0),
                ])
        }
        
        let signInButtonWidth = NSLayoutConstraint(item: loginButton, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: buttonWidths!)
        let signInButtonHeight = NSLayoutConstraint(item: loginButton, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.08, constant: 0)
        let signInButtonX = NSLayoutConstraint(item: loginButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let signInButtonY = NSLayoutConstraint(item: loginButton, attribute: .top, relatedBy: .equal, toItem: passwordBox, attribute: .bottom, multiplier: 1.0, constant: betweenButtonAndBox!)
        
        let createAccountX = NSLayoutConstraint(item: createAccountButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let createAccountY = NSLayoutConstraint(item: createAccountButton, attribute: .top, relatedBy: .equal, toItem: loginButton, attribute: .bottom, multiplier: 1.0, constant: betweenThings! - 5)
        
        NSLayoutConstraint.activate([usernameTextBoxWidth, usernameTextBoxX, usernameTextBoxY, usernameTextBoxHeight, usernameTextX, usernameTextY, passwordTextBoxWidth, passwordTextBoxX, passwordTextBoxY, passwordTextBoxHeight, passwordTextX, passwordTextY, closeButtonX, signInButtonWidth, signInButtonHeight, signInButtonX, signInButtonY, createAccountX, createAccountY])
    }
}

extension SignInController {
    
    @objc func handleLogin() {
        
        guard let email = emailBox.text, let password = passwordBox.text else {
            print("form is not valid")
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { user, error in
            if error == nil && user != nil {
                self.present(TabBarController(), animated: true, completion: nil)
            } else {
                print("error logging in. \(error!.localizedDescription)")
            }
        })
    }
}
