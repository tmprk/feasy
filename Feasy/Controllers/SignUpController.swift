//
//  SignUpController.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase

class SignUpController: UIViewController, UITextFieldDelegate {
    
    var ref: DatabaseReference!
    var shouldSetupConstraints = true
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    let closeButton: UIButton = {
        let button = UIButton(type: .custom) as UIButton
        button.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "closeButton")
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(singleTapping(recognizer:)), for: .touchUpInside)
        return button
    }()
    
    let usernameText: UILabel = {
        let usernameText = UILabel()
        usernameText.text = "Username"
        usernameText.textColor = UIColor.white
        usernameText.translatesAutoresizingMaskIntoConstraints = false
        return usernameText
    }()
    
    let usernameBox: UITextField = {
        let usernameBox = UITextField()
        usernameBox.autocorrectionType = .no
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        usernameBox.leftView = paddingView
        usernameBox.leftViewMode = .always
        usernameBox.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:0.3)
        usernameBox.textColor = UIColor.white
        usernameBox.tintColor = UIColor.clear
        usernameBox.textAlignment = .left
        usernameBox.layer.cornerRadius = 3
        usernameBox.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [.foregroundColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.8), .font: UIFont(name: "Avenir-Medium", size: 17) ?? UIFont.systemFont(ofSize: 17)])
        usernameBox.layer.borderWidth = 2.8
        usernameBox.layer.borderColor = UIColor.white.cgColor
        usernameBox.translatesAutoresizingMaskIntoConstraints = false
        return usernameBox
    }()
    
    let passwordText: UILabel = {
        let passwordText = UILabel()
        passwordText.text = "Password"
        passwordText.textColor = UIColor.white
        passwordText.translatesAutoresizingMaskIntoConstraints = false
        return passwordText
    }()
    
    let passwordBox: UITextField = {
        let passwordBox = UITextField()
        passwordBox.autocorrectionType = .no
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        passwordBox.isSecureTextEntry = true
        passwordBox.leftView = paddingView
        passwordBox.leftViewMode = .always
        passwordBox.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:0.3)
        passwordBox.textColor = UIColor.white
        passwordBox.tintColor = UIColor.clear
        passwordBox.textAlignment = .left
        passwordBox.layer.cornerRadius = 3
        passwordBox.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [.foregroundColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.8), .font: UIFont(name: "Avenir-Medium", size: 17) ?? UIFont.systemFont(ofSize: 17)])
        passwordBox.layer.borderWidth = 2.8
        passwordBox.layer.borderColor = UIColor.white.cgColor
        passwordBox.translatesAutoresizingMaskIntoConstraints = false
        return passwordBox
    }()
    
    let confirmPasswordText: UILabel = {
        let passwordText = UILabel()
        passwordText.text = "Confirm"
        passwordText.textColor = UIColor.white
        passwordText.translatesAutoresizingMaskIntoConstraints = false
        return passwordText
    }()
    
    let confirmPasswordBox: UITextField = {
        let passwordBox = UITextField()
        passwordBox.autocorrectionType = .no
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        passwordBox.isSecureTextEntry = true
        passwordBox.leftView = paddingView
        passwordBox.leftViewMode = .always
        passwordBox.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:0.3)
        passwordBox.textColor = UIColor.white
        passwordBox.tintColor = UIColor.clear
        passwordBox.textAlignment = .left
        passwordBox.layer.cornerRadius = 3
        passwordBox.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [.foregroundColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.8), .font: UIFont(name: "Avenir-Medium", size: 17) ?? UIFont.systemFont(ofSize: 17)])
        passwordBox.layer.borderWidth = 2.8
        passwordBox.layer.borderColor = UIColor.white.cgColor
        passwordBox.translatesAutoresizingMaskIntoConstraints = false
        return passwordBox
    }()
    
    let emailText: UILabel = {
        let emailtext = UILabel()
        emailtext.text = "Email"
        emailtext.textColor = UIColor.white
        emailtext.translatesAutoresizingMaskIntoConstraints = false
        return emailtext
    }()
    
    let emailBox: UITextField = {
        let emailBox = UITextField()
        emailBox.autocorrectionType = .no
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        emailBox.leftView = paddingView
        emailBox.leftViewMode = .always
        emailBox.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:0.3)
        emailBox.textColor = UIColor.white
        emailBox.tintColor = UIColor.clear
        emailBox.textAlignment = .left
        emailBox.layer.cornerRadius = 2.8
        emailBox.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [.foregroundColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.8), .font: UIFont(name: "Avenir-Medium", size: 17) ?? UIFont.systemFont(ofSize: 17)])
        emailBox.layer.borderWidth = 2.75
        emailBox.layer.borderColor = UIColor.white.cgColor
        emailBox.translatesAutoresizingMaskIntoConstraints = false
        return emailBox
    }()
    
    lazy var signUpButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system) as UIButton
        button.layer.borderWidth = 3.0
        button.setTitle("Sign Up", for: .normal)
        button.backgroundColor = UIColor.white
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 3.4
        button.setTitleColor(UIColor(red:0.27, green:0.46, blue:0.83, alpha:1.0), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleRegister), for: .touchUpInside)
        return button
    }()
    
    let forgotPassword: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.adjustsImageWhenHighlighted = false
        button.isUserInteractionEnabled = true
        button.layer.cornerRadius = 5.0
        button.setTitle("Forgot Password?", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(UIColor.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var iHaveAnAccount: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.adjustsImageWhenHighlighted = false
        button.isUserInteractionEnabled = true
        button.layer.cornerRadius = 5.0
        button.setTitle("I have an account!", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(UIColor.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
}

extension SignUpController {
    
    override func viewDidLoad() {
        ref = Database.database().reference()
        self.view.backgroundColor = UIColor(red:0.26, green:0.37, blue:0.76, alpha:1.0)
        snowRandomShit()
        
        usernameBox.delegate = self
        passwordBox.delegate = self
        emailBox.delegate = self
        confirmPasswordBox.delegate = self
        
        usernameBox.addTarget(self, action: #selector(addColorToBorder(textfield:)), for: .touchDown)
        passwordBox.addTarget(self, action: #selector(addColorToBorder(textfield:)), for: .touchDown)
        emailBox.addTarget(self, action: #selector(addColorToBorder(textfield:)), for: .touchDown)
        confirmPasswordBox.addTarget(self, action: #selector(addColorToBorder(textfield:)), for: .touchDown)
        layoutViews()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailBox {
            textField.resignFirstResponder()
            emailBox.bringBackToNormal()
            usernameBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            passwordBox.bringBackToNormal()
            confirmPasswordBox.bringBackToNormal()
            usernameBox.becomeFirstResponder()
        } else if textField == usernameBox {
            textField.resignFirstResponder()
            emailBox.bringBackToNormal()
            usernameBox.bringBackToNormal()
            passwordBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            confirmPasswordBox.bringBackToNormal()
            passwordBox.becomeFirstResponder()
        } else if textField == passwordBox {
            textField.resignFirstResponder()
            emailBox.bringBackToNormal()
            usernameBox.bringBackToNormal()
            passwordBox.bringBackToNormal()
            confirmPasswordBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            confirmPasswordBox.becomeFirstResponder()
        } else if textField == confirmPasswordBox {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @objc func cardTap(recognizer: UIGestureRecognizer) {
        self.emailBox.bringBackToNormal()
        self.passwordBox.bringBackToNormal()
        self.usernameBox.bringBackToNormal()
        self.confirmPasswordBox.bringBackToNormal()
        didEndEditing()
    }
    
    func didEndEditing() {
        for textField in self.view.subviews where textField is UITextField {
            textField.resignFirstResponder()
        }
    }
    
    @objc func addColorToBorder(textfield: UITextField) {
        if textfield == usernameBox {
            self.usernameBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            self.passwordBox.bringBackToNormal()
            self.emailBox.bringBackToNormal()
            self.confirmPasswordBox.bringBackToNormal()
        } else if textfield == passwordBox {
            self.passwordBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            self.usernameBox.bringBackToNormal()
            self.emailBox.bringBackToNormal()
            self.confirmPasswordBox.bringBackToNormal()
        } else if textfield == emailBox {
            self.emailBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            self.passwordBox.bringBackToNormal()
            self.usernameBox.bringBackToNormal()
            self.confirmPasswordBox.bringBackToNormal()
        } else if textfield == confirmPasswordBox {
            self.confirmPasswordBox.layer.borderColor = UIColor(red:0.86, green:0.64, blue:0.99, alpha:1.0).cgColor
            self.passwordBox.bringBackToNormal()
            self.usernameBox.bringBackToNormal()
            self.emailBox.bringBackToNormal()
        }
    }
    
    @objc func singleTapping(recognizer: UIGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.usernameBox.bringBackToNormal()
        self.passwordBox.bringBackToNormal()
        self.emailBox.bringBackToNormal()
        self.confirmPasswordBox.bringBackToNormal()
        self.view.endEditing(true)
    }
    
    func layoutViews() {
        self.view.addSubview(usernameText)
        self.view.addSubview(usernameBox)
        self.view.addSubview(passwordText)
        self.view.addSubview(passwordBox)
        self.view.addSubview(signUpButton)
        
        self.view.addSubview(emailText)
        self.view.addSubview(emailBox)
        self.view.addSubview(closeButton)
//        self.view.addSubview(confirmPasswordText)
//        self.view.addSubview(confirmPasswordBox)
        self.view.addSubview(forgotPassword)
        
        usernameText.translatesAutoresizingMaskIntoConstraints = false
        usernameBox.translatesAutoresizingMaskIntoConstraints = false
        passwordText.translatesAutoresizingMaskIntoConstraints = false
        passwordBox.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        emailText.translatesAutoresizingMaskIntoConstraints = false
        emailBox.translatesAutoresizingMaskIntoConstraints = false
        closeButton.translatesAutoresizingMaskIntoConstraints = false
//        confirmPasswordText.translatesAutoresizingMaskIntoConstraints = false
//        confirmPasswordBox.translatesAutoresizingMaskIntoConstraints = false
        forgotPassword.translatesAutoresizingMaskIntoConstraints = false
        
        var margin: CGFloat?
        var heightPadding: CGFloat?
        var usernameTopPadding: CGFloat?
        var textBoxHeight: CGFloat?
        var textboxWidth: CGFloat?
        var betweenThings: CGFloat?
        var closeButtonMargins: CGFloat?
        var buttonWidths: CGFloat?
        var betweenButtonAndBox: CGFloat?
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                margin = screenWidth / 8
                usernameTopPadding = screenHeight / 7
                heightPadding = screenHeight / 60
                textboxWidth = screenWidth - (2 * margin!)
                textBoxHeight = screenHeight / 15
                emailText.font = UIFont(name: "Avenir-Medium", size: 19) ?? UIFont.systemFont(ofSize: 19)
                emailBox.font = UIFont(name: "Avenir", size: 16) ?? UIFont.systemFont(ofSize: 16)
                usernameText.font = UIFont(name: "Avenir-Medium", size: 19) ?? UIFont.systemFont(ofSize: 19)
                usernameBox.font = UIFont(name: "Avenir", size: 16) ?? UIFont.systemFont(ofSize: 16)
                passwordText.font = UIFont(name: "Avenir-Medium", size: 19) ?? UIFont.systemFont(ofSize: 19)
                passwordBox.font = UIFont(name: "Avenir", size: 16) ?? UIFont.systemFont(ofSize: 16)
//                confirmPasswordText.font = UIFont.systemFont(ofSize: 19)
//                confirmPasswordBox.font = UIFont.systemFont(ofSize: 16)
                betweenThings = screenHeight / 36
                closeButtonMargins = (heightPadding! + 8)
                buttonWidths = self.view.frame.size.width / 2.2
                betweenButtonAndBox = self.view.frame.size.height / 9
                forgotPassword.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 14) ?? UIFont.systemFont(ofSize: 14)
                iHaveAnAccount.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 14) ?? UIFont.systemFont(ofSize: 14)
                signUpButton.titleLabel?.font = UIFont(name: "Avenir-Black", size: 16) ?? UIFont.systemFont(ofSize: 16, weight: .bold)
            case 1334:
                margin = screenWidth / 8
                usernameTopPadding = screenHeight / 7
                heightPadding = screenHeight / 60
                textboxWidth = screenWidth - (2 * margin!)
                textBoxHeight = screenHeight / 15
                emailText.font = UIFont(name: "Avenir-Medium", size: 21) ?? UIFont.systemFont(ofSize: 21)
                emailBox.font = UIFont(name: "Avenir", size: 18) ?? UIFont.systemFont(ofSize: 18)
                usernameText.font = UIFont(name: "Avenir-Medium", size: 21) ?? UIFont.systemFont(ofSize: 21)
                usernameBox.font = UIFont(name: "Avenir", size: 18) ?? UIFont.systemFont(ofSize: 18)
                passwordText.font = UIFont(name: "Avenir-Medium", size: 21) ?? UIFont.systemFont(ofSize: 21)
                passwordBox.font = UIFont(name: "Avenir", size: 18) ?? UIFont.systemFont(ofSize: 18)
//                confirmPasswordText.font = UIFont.systemFont(ofSize: 21)
//                confirmPasswordBox.font = UIFont.systemFont(ofSize: 18)
                betweenThings = screenHeight / 36
                closeButtonMargins = (heightPadding! + 8)
                buttonWidths = self.view.frame.size.width / 2.2
                betweenButtonAndBox = self.view.frame.size.height / 9
                forgotPassword.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 16) ?? UIFont.systemFont(ofSize: 16)
                iHaveAnAccount.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 16) ?? UIFont.systemFont(ofSize: 16)
                signUpButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20, weight: .bold)
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X, Xs")
                margin = screenWidth / 8.5
                usernameTopPadding = screenHeight / 5.7
                heightPadding = screenHeight / 90
                textboxWidth = screenWidth - (2 * margin!)
                textBoxHeight = screenHeight / 20
                emailText.font = .systemFont(ofSize: 24, weight: .bold)
                emailBox.font = .systemFont(ofSize: 15, weight: .bold)
                usernameText.font = .systemFont(ofSize: 24, weight: .bold)
                usernameBox.font = .systemFont(ofSize: 15, weight: .bold)
                passwordText.font = .systemFont(ofSize: 24, weight: .bold)
                passwordBox.font = .systemFont(ofSize: 15, weight: .bold)
//                confirmPasswordText.font = .systemFont(ofSize: 24, weight: .bold)
//                confirmPasswordBox.font = .systemFont(ofSize: 15, weight: .bold)
                betweenThings = screenHeight / 35
                closeButtonMargins = (heightPadding! + 8)
                buttonWidths = self.view.frame.size.width / 2.3
                betweenButtonAndBox = self.view.frame.height / 5.8
                signUpButton.titleLabel?.font = .systemFont(ofSize: 20, weight: .semibold)
                forgotPassword.titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
                iHaveAnAccount.titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
                signUpButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            case 2688:
                print("iPhone Xs Max")
            case 1792:
                print("iPhone Xr")
            default:
                print("unknown")
            }
        }
        
        let emailTextX = NSLayoutConstraint(item: emailText, attribute: .leading, relatedBy: .equal, toItem: usernameText, attribute: .leading, multiplier: 1.0, constant: 0)
        let emailTextY = NSLayoutConstraint(item: emailText, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: usernameTopPadding!)
        
        let emailTextBoxWidth = NSLayoutConstraint(item: emailBox, attribute: .width, relatedBy: .equal, toItem: usernameBox, attribute: .width, multiplier: 1, constant: 0)
        let emailTextBoxHeight = NSLayoutConstraint(item: emailBox, attribute: .height, relatedBy: .equal, toItem: usernameBox, attribute: .height, multiplier: 1.0, constant: 0)
        let emailTextBoxX = NSLayoutConstraint(item: emailBox, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let emailTextBoxY = NSLayoutConstraint(item: emailBox, attribute: .top, relatedBy: .equal, toItem: emailText, attribute: .bottom, multiplier: 1, constant: heightPadding!)
        
        
        let usernameTextBoxWidth = NSLayoutConstraint(item: usernameBox, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: textboxWidth!)
        let usernameTextBoxX = NSLayoutConstraint(item: usernameBox, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let usernameTextBoxY = NSLayoutConstraint(item: usernameBox, attribute: .top, relatedBy: .equal, toItem: usernameText, attribute: .bottom, multiplier: 1, constant: heightPadding!)
        let usernameTextBoxHeight = NSLayoutConstraint(item: usernameBox, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0, constant: textBoxHeight!)
        
        let usernameTextX = NSLayoutConstraint(item: usernameText, attribute: .leading, relatedBy: .equal, toItem: usernameBox, attribute: .leading, multiplier: 1, constant: 0)
        let usernameTextY = NSLayoutConstraint(item: usernameText, attribute: .top, relatedBy: .equal, toItem: emailBox, attribute: .bottom, multiplier: 1, constant: betweenThings!)
        
        let passwordTextBoxWidth = NSLayoutConstraint(item: passwordBox, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: textboxWidth!)
        let passwordTextBoxX = NSLayoutConstraint(item: passwordBox, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let passwordTextBoxY = NSLayoutConstraint(item: passwordBox, attribute: .top, relatedBy: .equal, toItem: passwordText, attribute: .bottom, multiplier: 1, constant: heightPadding!)
        let passwordTextBoxHeight = NSLayoutConstraint(item: passwordBox, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0, constant: textBoxHeight!)
        
        let passwordTextX = NSLayoutConstraint(item: passwordText, attribute: .leading, relatedBy: .equal, toItem: usernameText, attribute: .leading, multiplier: 1, constant: 0)
        let passwordTextY = NSLayoutConstraint(item: passwordText, attribute: .top, relatedBy: .equal, toItem: usernameBox, attribute: .bottom, multiplier: 1, constant: betweenThings!)
        
        let closeButtonX = NSLayoutConstraint(item: closeButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: closeButtonMargins!)
        
        if #available(iOS 11, *) {
            let guide = view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                closeButton.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0),
                ])
        }
        
//        let confirmPasswordTextBoxWidth = NSLayoutConstraint(item: confirmPasswordBox, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: textboxWidth!)
//        let confirmPasswordTextBoxX = NSLayoutConstraint(item: confirmPasswordBox, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
//        let confirmPasswordTextBoxY = NSLayoutConstraint(item: confirmPasswordBox, attribute: .top, relatedBy: .equal, toItem: confirmPasswordText, attribute: .bottom, multiplier: 1, constant: heightPadding!)
//        let confirmPasswordTextBoxHeight = NSLayoutConstraint(item: confirmPasswordBox, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0, constant: textBoxHeight!)
//
//        let confirmPasswordTextX = NSLayoutConstraint(item: confirmPasswordText, attribute: .leading, relatedBy: .equal, toItem: usernameText, attribute: .leading, multiplier: 1, constant: 0)
//        let confirmPasswordTextY = NSLayoutConstraint(item: confirmPasswordText, attribute: .top, relatedBy: .equal, toItem: passwordBox, attribute: .bottom, multiplier: 1, constant: betweenThings!)
        
        let signUpButtonWidth = NSLayoutConstraint(item: signUpButton, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: buttonWidths!)
        let signUpButtonHeight = NSLayoutConstraint(item: signUpButton, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.08, constant: 0)
        let signUpButtonX = NSLayoutConstraint(item: signUpButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let signUpButtonY = NSLayoutConstraint(item: signUpButton, attribute: .top, relatedBy: .equal, toItem: passwordBox, attribute: .bottom, multiplier: 1.0, constant: betweenButtonAndBox! - 15)
        
        let forgotPasswordX = NSLayoutConstraint(item: forgotPassword, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let forgotPasswordY = NSLayoutConstraint(item: forgotPassword, attribute: .top, relatedBy: .equal, toItem: signUpButton, attribute: .bottom, multiplier: 1.0, constant: betweenThings! - 12)
        
//        let iHaveAnAccountX = NSLayoutConstraint(item: iHaveAnAccount, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0)
//        let iHaveAnAccountY = NSLayoutConstraint(item: iHaveAnAccount, attribute: .top, relatedBy: .equal, toItem: forgotPassword, attribute: .bottom, multiplier: 1.0, constant: betweenThings! - 18)
        
        NSLayoutConstraint.activate([emailTextX, emailTextY, emailTextBoxWidth, emailTextBoxHeight, emailTextBoxX, emailTextBoxY, usernameTextBoxWidth, usernameTextBoxX, usernameTextBoxY, usernameTextBoxHeight, usernameTextX, usernameTextY, passwordTextBoxWidth, passwordTextBoxX, passwordTextBoxY, passwordTextBoxHeight, passwordTextX, passwordTextY, closeButtonX, signUpButtonWidth, signUpButtonHeight, signUpButtonX, signUpButtonY, forgotPasswordX, forgotPasswordY])
    }
}

extension SignUpController {
    
    @objc func handleRegister() {
        
        guard let name = usernameBox.text, let email = emailBox.text, let password = passwordBox.text else {
            print("form is not valid")
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (authResult, error) in
            if error == nil && authResult != nil {
                
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = name
                changeRequest?.commitChanges(completion: { error in
                    if error == nil {
                        print("user display name changed")
                    } else {
                        print("could not change displayname. \(error!.localizedDescription)")
                    }
                })
                
                guard let user = authResult?.user else { return }
                let usersReference = self.ref.child("users").child(user.uid)
                let values = ["username": name, "email": email]
                usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                    if err != nil {
                        print("\(err!.localizedDescription)")
                        return
                    } else {
                        self.present(TabBarController(), animated: true, completion: nil)
                    }
                })
                
                print("user created!")
            } else {
                print("error creating user. \(error!.localizedDescription)")
            }
        })
    }
}
