//
//  ChatViewController.swift
//  Feasy
//
//  Created by Timothy Park on 12/22/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

final class ChatController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let cellId = "cellId"
    let flowLayout = DSSCollectionViewFlowLayout()
    private var screenWidth = UIScreen.main.bounds.width
    private var screenHeight = UIScreen.main.bounds.height
    let estimateWidth = UIScreen.main.bounds.width * 0.95
    let cellMarginSize = 16.0
    
    lazy var listCollectionView: UICollectionView = {
        let CV = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
        CV.backgroundColor = .clear
        CV.translatesAutoresizingMaskIntoConstraints = false
        CV.register(ChatCell.self, forCellWithReuseIdentifier: cellId)
        CV.alwaysBounceVertical = true
        CV.showsVerticalScrollIndicator = false
        CV.decelerationRate = .fast
        CV.dataSource = self
        CV.delegate = self
        return CV
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareNavigation(title: "Chat", font: UIFont(name: "Pacifico-Regular", size: 23) ?? UIFont.systemFont(ofSize: 23, weight: .medium))
        self.setStatusBarStyle(.lightContent)
    }
}

extension ChatController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatCell
        cell.contentView.backgroundColor = UIColor(red:0.20, green:0.62, blue:0.60, alpha:1.0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let statusBarOrientation = UIApplication.shared.statusBarOrientation
        // it is important to do this after presentModalViewController:animated:
        if (statusBarOrientation != UIInterfaceOrientation.portrait
            && statusBarOrientation != UIInterfaceOrientation.portraitUpsideDown){
            screenHeight = UIScreen.main.bounds.size.width
        } else {
            screenHeight = UIScreen.main.bounds.size.height
        }
        let width = self.calculateWidth()
        return CGSize(width: width, height: screenHeight * 0.145)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //        let cellWidth: CGFloat = screenWidth * 0.436
        let cellWidth: CGFloat = self.calculateWidth()
        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        return UIEdgeInsets(top: edgeInsets, left: edgeInsets, bottom: edgeInsets, right: edgeInsets)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        // cannot exceed the edgeInsets number above or else changes rows
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //        return screenHeight * 0.015
        return CGFloat(cellMarginSize)
    }
    
}

extension ChatController {
    
    func setupConstraints() {
        view.backgroundColor = .white
        view.addSubview(listCollectionView)
        
        let cardViewConstraints = [
            listCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            listCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            listCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            listCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(cardViewConstraints)
    }
    
    func calculateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        return width
    }
    
}
