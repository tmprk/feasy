//
//  ProfileController.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit
import Firebase

class ProfileController: UIViewController {
    
    var ref: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        generalSetup()
        changeTabBarUsername()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareNavigation(title: "Profile", font: UIFont(name: "Avenir-Medium", size: 24) ?? UIFont.systemFont(ofSize: 23, weight: .medium))
    }
}

extension ProfileController {
    
    func changeTabBarUsername() {
        if checkIfUserIsSignedIn() {
            let uid = Auth.auth().currentUser?.uid
            ref.child("users").child(uid!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    guard let secondTabVCTitle = self.navigationController?.navigationBar.topItem else {
                        print("didn't get it")
                        return
                    }
                    secondTabVCTitle.title = "\(dictionary["username"] as! String)"
                }
            })
        } else {
            logout()
        }
    }
    
    func checkIfUserIsSignedIn() -> Bool {
        if Auth.auth().currentUser?.uid != nil {
            return true
        } else {
            logout()
            return false
        }
    }
    
    func generalSetup() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log Out", style: .plain, target: self, action: #selector(logout))
    }
    
    @objc func logout() {
        do {
            try Auth.auth().signOut()
            self.present(EntryController(), animated: false, completion: nil)
        } catch let error as NSError {
            print ("\(error.localizedDescription)")
        }
    }
}
