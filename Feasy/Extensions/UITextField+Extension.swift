//
//  UITextField+Extension.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

extension UITextField {
    func bringBackToNormal() {
        self.layer.borderColor = UIColor.white.cgColor
    }
}
