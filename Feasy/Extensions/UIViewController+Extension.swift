//
//  Navigation+Extension.swift
//  Feasy
//
//  Created by Timothy Park on 12/22/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func prepareNavigation(title: String, font: UIFont) {
        extendedLayoutIncludesOpaqueBars = true
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: font
        ]
        
        self.navigationController?.navigationBar.setTitleVerticalPositionAdjustment(2.5, for: .default)
        self.navigationController?.navigationBar.topItem?.title = title
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.20, green:0.62, blue:0.60, alpha:1.0)
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
        }
    }
    
}
