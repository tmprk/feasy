//
//  Emitter.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

class Emitter {
    
    static func get(with image: UIImage) -> CAEmitterLayer {
        let emitter = CAEmitterLayer()
        emitter.emitterShape = CAEmitterLayerEmitterShape.line
        emitter.emitterCells = generateEmitterCells(image: image)
        return emitter
    }
    
    static func generateEmitterCells(image: UIImage) -> [CAEmitterCell] {
        let intensity: Float = 0.5
        var cells = [CAEmitterCell]()
        let cell = CAEmitterCell()
        cell.contents = image.cgImage
        cell.emissionLongitude = (180 * (.pi / 180))
        cell.emissionRange = (45 * (.pi / 180))
        cell.birthRate = 6.0 * intensity
        cell.color = UIColor(red:0.76, green:0.53, blue:0.89, alpha:1.0).cgColor
        cell.lifetime = 14.0 * intensity
        cell.lifetimeRange = 3
        cell.velocity = CGFloat(350.0 * intensity)
        cell.velocityRange = CGFloat(80.0 * intensity)
        cell.spin = CGFloat(3.5 * intensity)
        cell.spinRange = CGFloat(4.0 * intensity)
        cell.scaleRange = CGFloat(intensity)
        cell.scaleSpeed = CGFloat(-0.1 * intensity)
        cells.append(cell)
        return cells
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
