//
//  ComposeView.swift
//  Feasy
//
//  Created by Xara on 12/2/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

class ComposeView: UIView {
    
    override func layoutSubviews() {
        layer.cornerRadius = 3.5
        let roundPath = UIBezierPath(roundedRect: bounds, cornerRadius: 3.5)
        let maskLayer = CAShapeLayer()
        maskLayer.path = roundPath.cgPath
        layer.mask = maskLayer
    }
    
}
