//
//  CellDelegate.swift
//  Feasy
//
//  Created by Timothy Park on 12/15/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import Foundation

protocol ExpandedCellDelegate: NSObjectProtocol {
    func topButtonTouched(indexPath: IndexPath)
}
