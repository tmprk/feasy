//
//  CustomCell.swift
//  Feasy
//
//  Created by Xara on 12/1/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var swipeGesture: UIPanGestureRecognizer!
    var originalPoint: CGPoint!
    
    public var indexPath: IndexPath!
    weak var delegate: ExpandedCellDelegate?
    weak var deleteDelegate: DeleteFunction?
    let screenWidth = UIScreen.main.bounds.width

    let toggleButton: UIButton = {
        let tb = UIButton(type: .system)
        tb.translatesAutoresizingMaskIntoConstraints = false
        tb.setTitleColor(.white, for: .normal)
        tb.setTitle("Expand", for: .normal)
        tb.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        tb.backgroundColor = UIColor(red:0.36, green:0.69, blue:0.55, alpha:1.0)
        tb.layer.cornerRadius = 5
        tb.titleLabel?.adjustsFontSizeToFitWidth = true
        tb.titleLabel?.minimumScaleFactor = 0.4
        return tb
    }()
    
    var timeLabel: UILabel = {
        let tl = UILabel(frame: .zero)
//        tl.backgroundColor = .gray
        tl.textColor = UIColor.white
        tl.textAlignment = .right
        tl.font = UIFont(name: "Avenir-Light", size: 13) ?? UIFont.systemFont(ofSize: 13)
        tl.translatesAutoresizingMaskIntoConstraints = false
        tl.adjustsFontSizeToFitWidth = true
        tl.minimumScaleFactor = 0.5
        return tl
    }()
    
    var contentLabel: UILabel = {
        let cl = UILabel(frame: .zero)
//        cl.backgroundColor = .red
        cl.textColor = UIColor.white
        cl.font = UIFont(name: "Avenir-Light", size: 15) ?? UIFont.systemFont(ofSize: 15)
        cl.numberOfLines = 0
        cl.lineBreakMode = .byWordWrapping
        cl.translatesAutoresizingMaskIntoConstraints = false
        return cl
    }()
    
    @objc func topButtonTouched() {
        if let delegate = self.delegate {
            delegate.topButtonTouched(indexPath: indexPath)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 3.5
        let roundPath = UIBezierPath(roundedRect: bounds, cornerRadius: 3.5)
        let maskLayer = CAShapeLayer()
        maskLayer.path = roundPath.cgPath
        layer.mask = maskLayer
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSwipeGesture()
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSwipeGesture()
        setupView()
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        contentLabel.preferredMaxLayoutWidth = layoutAttributes.size.width - contentView.layoutMargins.left - contentView.layoutMargins.left
        layoutAttributes.bounds.size.height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        return layoutAttributes
    }
    
    func setupView() {
        self.contentView.addSubview(timeLabel)
        self.contentView.addSubview(contentLabel)
        
//        self.contentView.addSubview(toggleButton)
//        toggleButton.addTarget(self, action: #selector(topButtonTouched), for: .touchDown)
        
        let labelInset = UIEdgeInsets(top: 10, left: 10, bottom: -10, right: -10)
        
        let menuAndToggleConstraints = [
            
            timeLabel.topAnchor.constraint(equalTo: contentLabel.bottomAnchor, constant: labelInset.top),
            timeLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: labelInset.left),
            timeLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: labelInset.right),
            timeLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor, constant: labelInset.bottom),
            timeLabel.heightAnchor.constraint(equalToConstant: 16),
            
            contentLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor, constant: labelInset.top),
            contentLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: labelInset.left),
            contentLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: labelInset.right)
            
//            contentLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: (screenWidth - width) / 2),
//            contentLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 10),
//            locationLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: (screenWidth - width) / 2),
//            locationLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: -13),
//            toggleButton.topAnchor.constraint(equalTo: self.topAnchor, constant: width / 20),
//            toggleButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -(screenWidth - width) / 2),
//            toggleButton.widthAnchor.constraint(equalToConstant: 100)
        ]
        NSLayoutConstraint.activate(menuAndToggleConstraints)
    }
    
    func setupSwipeGesture() {
        swipeGesture = UIPanGestureRecognizer(target: self, action:#selector(swiped(_:)))
        swipeGesture.delegate = self
        self.addGestureRecognizer(swipeGesture)
    }
    
    @objc func swiped(_ gestureRecognizer: UIPanGestureRecognizer) {
        let xDistance:CGFloat = gestureRecognizer.translation(in: self).x
        
        switch(gestureRecognizer.state) {
        case UIGestureRecognizer.State.began:
            self.originalPoint = self.center
        case UIGestureRecognizer.State.changed:
            let translation: CGPoint = gestureRecognizer.translation(in: self)
            let displacement: CGPoint = CGPoint.init(x: translation.x, y: translation.y)
            
            if displacement.x + self.originalPoint.x < self.originalPoint.x {
                self.center.x += translation.x
                self.center = CGPoint(x: self.originalPoint.x + xDistance, y: self.originalPoint.y)
            }
        case UIGestureRecognizer.State.ended:
            let hasMovedToFarLeft = self.frame.maxX < UIScreen.main.bounds.width / 1.5
            if (hasMovedToFarLeft) {
                removeViewFromParentWithAnimation()
            } else {
                resetViewPositionAndTransformations()
            }
        default:
            break
        }
    }
    
    func resetViewPositionAndTransformations(){
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIView.AnimationOptions(), animations: {
            self.center = self.originalPoint
            self.transform = CGAffineTransform(rotationAngle: 0)
        }, completion: {success in })
    }
    
    func removeViewFromParentWithAnimation() {
        var animations:(() -> Void)!
        animations = {self.center.x = -self.frame.width}
        UIView.animate(withDuration: 0.2, animations: animations, completion: {success in
            if let delegate = self.deleteDelegate {
                delegate.cellDelete(indexPath: self.indexPath)
            }
            self.removeFromSuperview()
        })
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return abs((swipeGesture.velocity(in: swipeGesture.view)).x) > abs((swipeGesture.velocity(in: swipeGesture.view)).y)
    }
    
    override func snapshotView(afterScreenUpdates afterUpdates: Bool) -> UIView? {
        let snapshot = super.snapshotView(afterScreenUpdates: afterUpdates)
        snapshot?.layer.masksToBounds = false
        snapshot?.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        snapshot?.layer.shadowRadius = 5.0
        snapshot?.layer.shadowOpacity = 0.4
        snapshot?.center = center
        return snapshot
    }
}
