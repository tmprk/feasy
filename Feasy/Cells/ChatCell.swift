//
//  ChatCell.swift
//  Feasy
//
//  Created by Timothy Park on 12/22/18.
//  Copyright © 2018 Timothy Park. All rights reserved.
//

import UIKit

class ChatCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 3.5
        let roundPath = UIBezierPath(roundedRect: bounds, cornerRadius: 3.5)
        let maskLayer = CAShapeLayer()
        maskLayer.path = roundPath.cgPath
        layer.mask = maskLayer
    }

}
