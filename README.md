# Simple feedback app

send and receive messages in Swift 4.2 and Xcode 10

## Screenshots

| entry | login | inbox | compose |
|---|---|---|---|
| <img src="https://github.com/jstarvalentine/Feasy/blob/master/Screenshots/1.png" height="350">  | <img src="https://github.com/jstarvalentine/Feasy/blob/master/Screenshots/2.png" height="350">  | <img src="https://github.com/jstarvalentine/Feasy/blob/master/Screenshots/3.png" height="350">  | <img src="https://github.com/jstarvalentine/Feasy/blob/master/Screenshots/4.png" height="350">  |

## Demo
<!-- <p style="text-align: center;">
	![](gifgifgif)
</p>
 -->

<p align="center">
  <img src="gifgif.gif">
</p>
